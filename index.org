#+TITLE: BFS EXPERIMENT DASHBOARD
#+AUTHOR: VLEAD
#+DATE: 
#+SETUPFILE: org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil'

BFS experiment : Summer 2019 internship

* Introduction
  This experiment will introduce the learner to the
  BFS algorithm.

* Hosted Link
  Hosted Link for the BFS experiment :: [[http://exp-iiith.vlabs.ac.in/bfs/exp.html][Breadth First Search]]

* Scope of the project
  This experiment will be very essential to students as it
  will introduce them to a very fundamental and important
  topic of the algorithms course. This will enable students
  to understand and implement BFS.
  
* Project Developers and Contributors Details
  |------------------------+-----------------------+-----------+------------+------------------------+------------------|
  | *Names*                | *Year of Study*       | *Role*    | *Phone-No* | *Email-ID*             | *gitlab handles* |
  |------------------------+-----------------------+-----------+------------+------------------------+------------------|
  | Kalakonda Sai Shashank | 1yr year Btech IIIT-H | Developer | 9121485027 | shashank0225@gmail.com | chinnu_sai25     |
  |------------------------+-----------------------+-----------+------------+------------------------+------------------|

* References
  A mapping between different documents and their roles.
  |--------+-------------------------+---------------------------------------------------------------------|
  | *S.NO* | *Link*                  | *Role*                                                              |
  |--------+-------------------------+---------------------------------------------------------------------|
  |     1. | [[./structure.org][Structure]]               | Captures the structure of experiment content                        |
  |     2. | [[./design-spec.org][Design Spec]]             | Captures the design specs of the experiment structure and artefacts |
  |     3. | [[https://gitlab.com/vlead-projects/experiments/ds/breadth-first-search/content-html][Content]]                 | Captures the experiment content                                     |
  |     4. | [[https://gitlab.com/vlead-projects/experiments/ds/breadth-first-search/artefacts][Artefacts]]               | This is where the artefacts of the experiments are implemented      |
  |     5. | [[https://docs.google.com/document/d/1DdRnoZguDg4jJkOk-D5MUDr7mCATExBVw6QSi1XioG0/edit][Deployment-Instructions]] | This document describes (build process and changes) reducing the manual work in each experiment deployment process and changes which we have made to reduce manual work.                                                                    |
  |--------+-------------------------+---------------------------------------------------------------------|

* Assumptions
  /User's knowledge assumptions/ 
  + Mathematics,English
  + Sorting, Time Complexity, Arrays

* StakeHolders
  + College and School Students
  + MHRD
  + Teachers

* Value Added by our project
  + It would be beneficial for 1st and 2nd year engineering
    students as Data Structures and Algorithms are usually
    thought in these years of study.
  + Highly beneficial for tier 2 and tier 3 college students
    who can use this to learn and understand the concept of
    linked lists.

* Risks and Challenges
  + One challenge was to make artefacts in such a way that
    they can be rendered properly by the existing
    infrastructure in a proper manner which was quite rigid.
  + Couldn't add some kinds of media like <iframe> initially,
    raised issue with the infra team.
 
* Learnings
  + Literate programming
  + Understanding different methods of learning used by
    people and make an application that caters to all.


 
